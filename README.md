<h1 align="center">Welcome to HeroCLI 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0-blue.svg?cacheSeconds=2592000" />
</p>

> A simple CLI application written in Java to create, level, and equip heroes

                                      /|
                                     |\|
                                     |||
                                     |||
                                     |||
                                     |||
                                     |||
                                     |||
                                  ~-[{o}]-~
                                     |/|
                                     |/|
             ///~`     |\\_          `0'         =\\\\         . .
            ,  |='  ,))\_| ~-_                    _)  \      _/_/|
           / ,' ,;((((((    ~ \                  `~~~\-~-_ /~ (_/\
         /' -~/~)))))))'\_   _/'                      \_  /'  D   |
        (       (((((( ~-/ ~-/                          ~-;  /    \--_
         ~~--|   ))''    ')  `                            `~~\_    \   )
             :        (_  ~\           ,                    /~~-     ./

_ASCII art by Tua Xiong_

## Usage

```
Requires: JDK16, JUnit

(1) DEPENDENCIES AND PRECONFIG
-> Add JDK16 (v16.02 used for development), 
-> Add junit:junit:4.13.1 to path for testing
-> Add org.junit.jupiter:junit-jupiter:5.7.0 to path for testing
-> Declare src as root folder

(2) RUNNING HeroCLI
GO TO src/herocli/Main --->
RUN Main.main() in IDE or compile via CLI if desired
```

## Run tests

```sh
All tests are run inside of the tests folder.
```

Usage:
- Choose a character class
- Choose a username
- View lists of armor/weapons
- Show stats
- Level up
- Equip items
- Automated testing


Limitations:
- CLI only, no GUI
- No information is saved upon quitting the app
- Ideally, all fields should be private with getters and setters. There are 
some cases, enums and objects, in the item class, that require getter and setter
functionality



***
_END OF README FILE_



