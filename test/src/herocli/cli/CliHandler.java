package herocli.cli;

import herocli.hero.Character;
import herocli.hero.characters.Mage;
import herocli.hero.characters.Ranger;
import herocli.hero.characters.Rogue;
import herocli.hero.characters.Warrior;
import herocli.item.lists.ArmorList;
import herocli.item.lists.WeaponList;

import java.util.Scanner;
public class CliHandler {

    public boolean start() throws Character.InvalidWeaponException, Character.InvalidArmorException {
        //create new weapon and armor list classes
        WeaponList weapons = new WeaponList();
        ArmorList armor = new ArmorList();

        //create scanner object
        Scanner scanner = new Scanner(System.in);

        //print welcome message
        System.out.println(" Welcome Adventurer! \n"
                + " Please select your hero class by entering a number. \n"
                + " 0 => Mage \n"
                + " 1 => Warrior \n"
                + " 2 => Rogue \n"
                + " 3 => Ranger \n");

        //get user input for which hero class they want to pick
        String classType;
        while (true) {
            classType = scanner.nextLine();
            if (classType.matches("[0-3]")) {
                break;
            } else {
                System.out.println("Please enter a valid number input e.g. 0 \n");
            }
        }

        //generate the user's hero class based on previous input
        Character character = switch (classType) {
            case "0" -> character = new Mage();
            case "1" -> character = new Warrior();
            case "2" -> character = new Rogue();
            case "3" -> character = new Ranger();
            default -> throw new RuntimeException();
        };

        //print the result of hero generation to console
        System.out.println("Created new character type:");
        System.out.println(character +"\n");

        //proceed by asking the user to input their name
        System.out.println("What is your name adventurer?\n");
        String name;
        while (true) {
            name = scanner.nextLine();
            //validate name input (2-20 chars, only int and chars allowed)
            if (name.matches("[0-9a-zA-Z]{2,20}$")) {
                break;
            } else {
                //error prompt to user that entered invalid input
                System.out.println("Please enter a valid name: 2-20 characters, letters and numbers only.\n");
            }
        }
        //set the name
        character.setName(name);
        //print a message including the name to the console
        System.out.printf("Welcome to the realm %s. Adventures await.\n", character.getName());

        //specify options menu for actions
        String optionsMenu = ("Please select a valid action by entering a number: \n"
                + " 0 => Quit \n"
                + " 1 => Level up \n"
                + " 2 => Stats report \n"
                + " 3 => Weapon list \n"
                + " 4 => Armor list \n"
                + " 5 => Equip weapon \n"
                + " 6 => Equip armor \n"
                + " 7 => Options menu");
        System.out.println(optionsMenu);

        //initialize action type for options menu logic
        String actionType;

        //loop infinitely
        while (true) {

            //prompt user to specify desired action
            while (true) {
                actionType = scanner.nextLine();
                //check if action is valid
                if (actionType.matches("[0-7]")) {
                    break;
                } else {
                    System.out.println("Please select a valid action by entering a number. Enter 7 for options menu. \n");
                }
            }

            switch (actionType) {
                //quit the app
                case "0" -> {
                    System.out.println("Quitting the application...");
                    // Exit by returning the function herocli.main.CliHandler.start()
                    return false;
                }
                //level up
                case "1" -> {
                    character.levelUp();
                    System.out.printf("Congratulations, your character has reached level %s. \n", character.getLevel());
                }
                //generate report
                case "2" -> {
                    System.out.printf("Here is a stat report of your %s character: \n", character);
                    character.statReport();
                }
                //list available weapons
                case "3" -> {
                    System.out.println("~~~~~~~~~~~~~~~~~~");
                    System.out.println("LIST OF WEAPONS: ");
                    weapons.printList();
                    System.out.println("~~~~~~~~~~~~~~~~~~");
                }
                //list available armor
                case "4" -> {
                    System.out.println("~~~~~~~~~~~~~~~~~~");
                    System.out.println("LIST OF ARMOR: ");
                    armor.printList();
                    System.out.println("~~~~~~~~~~~~~~~~~~");
                }
                //logic to allow user to equip a weapon
                case "5" -> {
                    int weaponIndex;
                    //prompt user
                    System.out.println("Which weapon would you like to equip? Enter a number. (refer to weapon list) \n");

                    while (true) {
                        actionType = scanner.nextLine();
                        //if the input matches any number
                        if (actionType.matches("[0-9]+")) {
                            //use the input as an index and proceed by breaking
                            weaponIndex = Integer.parseInt(actionType);
                            break;
                        } else {
                            System.out.println("Please select a valid action by entering a number. \n");
                        }
                    }

                    //if the weapon does not exist...
                    if (weapons.weaponList.get(weaponIndex) == null) {
                        System.out.println("Item does not exist. For valid number inputs, refer to the weapons list.");;
                    }
                    //else try to equip it...
                    else if (character.equipWeapon(weapons.weaponList.get(weaponIndex))) {
                        System.out.printf("Successfully equipped the weapon %s \n", weapons.weaponList.get(weaponIndex).name);
                    }
                }
                //logic to allow user to equip an armor piece
                case "6" -> {
                    int armorIndex;
                    System.out.println("Which armor would you like to equip? Enter a number. (refer to armor list) \n");

                    //prompt user
                    while (true) {
                        actionType = scanner.nextLine();
                        //if the input matches any number
                        if (actionType.matches("[0-9]+")) {
                            //use the input as an index and proceed by breaking
                            armorIndex = Integer.parseInt(actionType);
                            break;
                        } else {
                            System.out.println("Please select a valid action by entering a number. \n");
                        }
                    }

                    //if the armor does not exist...
                    if (armor.armorList.get(armorIndex) == null) {
                        System.out.println("Item does not exist. For valid number inputs, refer to the weapons list.");;
                    }
                    //else try to equip it...
                    else if (character.equipArmor(armor.armorList.get(armorIndex))) {
                        System.out.printf("Successfully equipped the weapon %s \n", armor.armorList.get(armorIndex).name);
                    }
                }
                //print the options menu
                case "7" -> System.out.println(optionsMenu);
            }

            //indicate that the application is idle to the user
            System.out.println();
            System.out.println(" ~ awaiting orders ~ ");
            System.out.println();

        }
    }
}
