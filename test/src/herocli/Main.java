package herocli;
import herocli.cli.CliHandler;
import herocli.hero.Character;

public class Main {
    public static void main(String[] args) throws Character.InvalidArmorException, Character.InvalidWeaponException {
        CliHandler program = new CliHandler();
        program.start();
    }
}
