package herocli.hero;

import herocli.item.Armor;
import herocli.item.Item;
import herocli.item.Weapon;

import java.util.HashMap;

abstract public class Character {

    //initializing character's primary attribute
    public PrimaryAttribute primaryAttribute = new PrimaryAttribute(0,0,0,0);

    //level of character
    private int level;

    //name of character
    private String name;

    //hashmap of worn equipment
    private HashMap<Item.Slot, Item> wornEquipment = new HashMap<>();

    //getter for primary attribute
    public PrimaryAttribute getPrimaryAttribute() {
        return primaryAttribute;
    }

    //getter for total attribute
    public PrimaryAttribute getTotalAttribute() {
        return combinePrimaryAttributes(getWornArmorAttributes(), primaryAttribute);
    }

    // Method used to combine multiple types of herocli.hero.PrimaryAttribute for calculating total stats
    // and combined armor stats.
    public PrimaryAttribute combinePrimaryAttributes(PrimaryAttribute... primaryAttributes) {
        //initializing default return values
        int vitality = 0;
        int strength = 0;
        int dexterity = 0;
        int intelligence = 0;

        //calculating total return values
        for (PrimaryAttribute primaryAttribute: primaryAttributes) {
            vitality += primaryAttribute.getVitality();
            strength += primaryAttribute.getStrength();
            dexterity += primaryAttribute.getDexterity();
            intelligence += primaryAttribute.getIntelligence();
        }

        //returning a new herocli.hero.PrimaryAttribute object that describes the combined total
        //of the input
        return new PrimaryAttribute(vitality, strength, dexterity, intelligence);
    }

    protected PrimaryAttribute getWornArmorAttributes() {
        //initializing variables
        PrimaryAttribute head = new PrimaryAttribute(0,0,0,0);
        PrimaryAttribute body = new PrimaryAttribute(0,0,0,0);
        PrimaryAttribute legs = new PrimaryAttribute(0,0,0,0);

        //typecasting hashmap of items into appropriate child type (herocli.item.Item -> herocli.item.Armor)
        //then fetching their herocli.hero.PrimaryAttribute

        //if something is worn on head slot...
        if (wornEquipment.get(Item.Slot.HEAD) != null) {
            head = ((Armor) wornEquipment.get(Item.Slot.HEAD)).getArmorAttribute();
        }

        //if something is worn on body...
        if (wornEquipment.get(Item.Slot.BODY) != null) {
            body = ((Armor) wornEquipment.get(Item.Slot.BODY)).getArmorAttribute();
        }

        //if something is worn on legs...
        if (wornEquipment.get(Item.Slot.LEGS) != null) {
            legs = ((Armor) wornEquipment.get(Item.Slot.LEGS)).getArmorAttribute();
        }

        //return total combined armor attributes
        return combinePrimaryAttributes(head, body, legs);
    }

    protected double getWornWeaponDPS() {
        //no weapon is worn, then DPS modifier is 1
        if (wornEquipment.get(Item.Slot.WEAPON) == null) {
            return 1.0;
        } else {
            //typecasting hashmap of items into appropriate child type
            Weapon currentWeapon = (Weapon) wornEquipment.get(Item.Slot.WEAPON);
            return (currentWeapon.getWeaponDPS());
        }
    }

    public double getCharacterDPS() {
        //get total characterDPS with formula using weaponDPS and totalAttribute
        return getWornWeaponDPS() * (1 + (getTotalAttribute().getStrength()) / 100.0);
    }

    //set the name
    public void setName(String name) {
        this.name = name;
    }

    //get the name
    public String getName() {
        return name;
    }

    //set the worn equipment, useful for equipping new items
    protected void setWornEquipment(Item.Slot slot, Item item) {
        this.wornEquipment.put(slot, item);
    }

    //get the level
    public int getLevel() {
        return level;
    }

    //set the level
    protected void setLevel(int level) {
        this.level = level;
    }

    //constructor
    public Character() {
        this.level = 1;
        this.name = "";
    }

    //custom exception class that extends the Exception class
    public static class InvalidWeaponException extends Exception {
        public InvalidWeaponException (String message)
        {
            // calling the constructor of parent Exception
            super(message);
        }
    }

    //custom exception class that extends the Exception class
    public static class InvalidArmorException extends Exception {
        public InvalidArmorException (String message)
        {
            // calling the constructor of parent Exception
            super(message);
        }
    }

    //weapon handler that throws exceptions for invalid actions
    public boolean equipWeaponHandler(String heroType, Weapon item,
                               Weapon.WeaponType... characterWeaponTypes)
            throws InvalidWeaponException {
        try {
            // Check if the level required is sufficient to equip the item
            if (getLevel() < item.requiredLevel) throw new InvalidWeaponException(
                    "You need to be level "
                            + item.requiredLevel
                            + " to equip the "
                            + item.name);

            // Check if the character can equip this item type
            // by looping through the character's weapon types
            for (Weapon.WeaponType weaponType: characterWeaponTypes) {
                if (weaponType == item.weaponType) {
                    setWornEquipment(item.slot, item);
                    return true;
                }
            }

            // Equipping did not succeed, throwing error
            throw new InvalidWeaponException(heroType
                    + " cannot equip "
                    + item.weaponType);

        } catch (InvalidWeaponException exception) {
            // printing the message from InvalidWeaponException object
            System.out.println(exception);
            // returning false to indicate the item was not equipped
            return false;
        }
    }

    //equip armor handler that throws exceptions for invalid armor equips
    public boolean equipArmorHandler(String heroType, Armor item,
                               Armor.ArmorType... characterArmorTypes)
            throws InvalidArmorException {
        try {
            // Check if the level required is sufficient to equip the item
            if (getLevel() < item.requiredLevel) throw new InvalidArmorException(
                    "You need to be level "
                            + item.requiredLevel
                            + " to equip the "
                            + item.name);

            // Check if the character can equip this item type
            // by looping through the character's weapon types
            for (Armor.ArmorType armorType: characterArmorTypes) {
                if (armorType == item.armorType) {
                    setWornEquipment(item.slot, item);
                    return true;
                }
            }

            // Equipping did not succeed, throwing error
            throw new InvalidArmorException(heroType
                    + " cannot equip "
                    + item.armorType);

        } catch (InvalidArmorException exception) {
            // printing the message from InvalidWeaponException object
            System.out.println(exception);
            // returning false to indicate the item was not equipped
            return false;
        }
    }

    //function to generate a stat report
    //and print it to the console
    public void statReport() {
        StringBuilder stats = new StringBuilder();
        stats.append(String.format("Name = %s \n", this.name));
        stats.append(String.format("Level = %s \n", this.level));
        stats.append(String.format("Strength = %s \n", primaryAttribute.getStrength() + getWornArmorAttributes().getStrength()));
        stats.append(String.format("Dexterity = %s \n", primaryAttribute.getDexterity() + getWornArmorAttributes().getDexterity()));
        stats.append(String.format("Intelligence = %s \n", primaryAttribute.getIntelligence() + getWornArmorAttributes().getIntelligence()));
        stats.append(String.format("Vitality = %s \n", primaryAttribute.getVitality() + getWornArmorAttributes().getVitality()));
        stats.append(String.format("DPS = %s", getCharacterDPS()));
        System.out.println(stats);
    }

    //abstract methods to be found in child class e.g. herocli.hero.characters.Mage, herocli.hero.characters.Warrior, herocli.hero.characters.Rogue, herocli.hero.characters.Ranger
    abstract public void levelUp();
    abstract public boolean equipWeapon(Weapon item) throws InvalidWeaponException;
    abstract public boolean equipArmor(Armor item) throws InvalidArmorException;

}
