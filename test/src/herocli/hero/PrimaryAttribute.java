package herocli.hero;

public class PrimaryAttribute {
    public int getVitality() {
        return vitality;
    }

    public void setVitality(int vitality) {
        this.vitality = vitality;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public void setPrimaryAttribute(int vitality, int strength, int dexterity, int intelligence) {
        this.vitality = vitality;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    private int vitality;
    private int strength;
    private int dexterity;
    private int intelligence;

    public PrimaryAttribute(int vitality, int strength, int dexterity, int intelligence) {
        this.vitality = vitality;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    // String method for complex object helps
    // with testing
    @Override
    public String toString() {
        return "herocli.hero.PrimaryAttribute{" +
                "vitality=" + vitality +
                ", strength=" + strength +
                ", dexterity=" + dexterity +
                ", intelligence=" + intelligence +
                '}';
    }
}
