package herocli.hero.characters;

import herocli.hero.Character;
import herocli.hero.PrimaryAttribute;
import herocli.item.Armor;
import herocli.item.Weapon;

public class Mage extends Character {

    private final String heroType = "Mage";
    // Attributes gained on levelling
    private final PrimaryAttribute levelBonus = new PrimaryAttribute(3,1,1,5);

    public Mage() {
        super();
        // Starting attributes
        primaryAttribute.setPrimaryAttribute(5, 1, 1, 8);
    }

    public void levelUp() {
        setLevel(getLevel() + 1);
        primaryAttribute = combinePrimaryAttributes(levelBonus, primaryAttribute);
    }

    // Wrapper method for herocli.hero.Character class equipWeaponHandler
    public boolean equipWeapon(Weapon item) throws InvalidWeaponException {
        return equipWeaponHandler(heroType,
                item,
                // declaring valid weapon types
                Weapon.WeaponType.STAFF,
                Weapon.WeaponType.WAND);
    }

    // Wrapper method for herocli.hero.Character class equipArmorHandler
    public boolean equipArmor(Armor item) throws InvalidArmorException {
        return equipArmorHandler(heroType,
                item,
                // declaring valid armor types
                Armor.ArmorType.CLOTH);
    }

    //toString() override to print the class
    @Override
    public String toString() {
        return heroType;
    }
}
