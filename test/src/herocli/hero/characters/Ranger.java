package herocli.hero.characters;

import herocli.hero.Character;
import herocli.hero.PrimaryAttribute;
import herocli.item.Armor;
import herocli.item.Weapon;

public class Ranger extends Character {

    private final String heroType = "Ranger";
    private final PrimaryAttribute levelBonus = new PrimaryAttribute(2,1,5,1);

    public Ranger() {
        super();
        primaryAttribute.setPrimaryAttribute(8,1,7,1);
    }

    public void levelUp() {
        setLevel(getLevel() + 1);
        primaryAttribute = combinePrimaryAttributes(levelBonus, primaryAttribute);
    }

    // Wrapper method for herocli.hero.Character class equipWeaponHandler
    public boolean equipWeapon(Weapon item) throws InvalidWeaponException {
        return equipWeaponHandler(heroType,
                item,
                // declaring valid weapon types
                Weapon.WeaponType.BOW);
    }

    // Wrapper method for herocli.hero.Character class equipArmorHandler
    public boolean equipArmor(Armor item) throws InvalidArmorException {
        return equipArmorHandler(heroType,
                item,
                // declaring valid armor types
                Armor.ArmorType.MAIL,
                Armor.ArmorType.LEATHER);
    }

    //toString() override to print the class
    @Override
    public String toString() {
        return heroType;
    }
}
